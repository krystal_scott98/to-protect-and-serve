﻿using UnityEngine;
using System.Collections;

public class VilligerMovement : MonoBehaviour {

	public float moveSpeed;

	private Rigidbody2D myRigidbody;
	private int WalkDirection;
	public bool isWalking;
	public float walkTime;
	private float walkCounter;
	public float waitTime;
	private float waitCounter;
	public Collider2D walkZone;

	private Vector2 minWalkPoint;
	private Vector2 maxWalkPoint;
	private bool hasWalkZone;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody2D> ();

		waitCounter = waitTime;
		walkCounter = walkTime;
		ChooseDirection ();

		if (walkZone != null) {
			minWalkPoint = walkZone.bounds.min;
			maxWalkPoint = walkZone.bounds.max;
			hasWalkZone = true;
		}
	}
	// Update is called once per frame
	void Update () {
		if (isWalking) {
			walkCounter -= Time.deltaTime;


			switch (WalkDirection) {
			case 0: 
				myRigidbody.velocity = new Vector2 (0, moveSpeed);
				if (hasWalkZone && transform.position.y> maxWalkPoint.y){
					isWalking = false;
					waitCounter = waitTime;
				}
				break;
			case 1: 
				myRigidbody.velocity = new Vector2 (moveSpeed, 0);
				if (hasWalkZone && transform.position.x> maxWalkPoint.x){
					isWalking = false;
					waitCounter = waitTime;
				}
				break;
			case 2: 
				myRigidbody.velocity = new Vector2 (0, -moveSpeed);
				if (hasWalkZone && transform.position.x < minWalkPoint.x){
					isWalking = false;
					waitCounter = waitTime;
				}
				break;
			case 3: 
				myRigidbody.velocity = new Vector2 (-moveSpeed, 0);
				if (hasWalkZone && transform.position.y < minWalkPoint.y){
					isWalking = false;
					waitCounter = waitTime;
				}
				break;


			}
			if (walkCounter < 0) {
				isWalking = false;
				waitCounter = waitTime;
			}
		}
			else{
				waitCounter -= Time.deltaTime;
				if (waitCounter < 0){
					ChooseDirection();
				}
			}
		

	}

	public void ChooseDirection(){
		WalkDirection = Random.Range (0, 4);
		isWalking = true;
		walkCounter = waitTime;
	}
}
