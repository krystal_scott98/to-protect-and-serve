﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DialogueManager : MonoBehaviour {

	public GameObject dBox;
	public Text dText;

	public string[] dialogLines;
	public int currentLine;

	public bool dialogueActive;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (dialogueActive && Input.GetKeyDown (KeyCode.Space)) {
			//dBox.SetActive(false);
			//dialogueActive = false;
			currentLine++;
		}
		if(currentLine >= dialogLines.Length)
		{
			dBox.SetActive(false);
			dialogueActive = false;
			currentLine = 0;
		}
		dText.text = dialogLines[currentLine];
	}
	public void ShowBox(string dialogue){
		dialogueActive = true;
		dBox.SetActive(true);
		dText.text = dialogue;
	}
	public void ShowDialogue(){
		dialogueActive = true;
		dBox.SetActive(true);
	}
}
