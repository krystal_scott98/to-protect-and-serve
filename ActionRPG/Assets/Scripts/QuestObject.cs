﻿using UnityEngine;
using System.Collections;

public class QuestObject : MonoBehaviour {

    public int questNumber;
    public QuestManager thQM;

    public string startText;
    public string endText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void StartQuest()
    {
        thQM.ShowQuestText(startText);
    }

    public void EndQuest()
    {
        thQM.ShowQuestText(endText);
        thQM.questCompleted[questNumber] = true;
           gameObject.SetActive(false);
    }
}
